use std::io::{self, BufRead, BufReader, Write};

fn main() -> io::Result<()> {
    print!("What is your name? ");
    io::stdout().flush().unwrap();

    let mut reader = BufReader::new(io::stdin());
    let mut buffer = String::new();
    reader.read_line(&mut buffer)?;

    buffer.pop();
    println!("Hello, {}, nice to meet you!", buffer);
    Ok(())
}
