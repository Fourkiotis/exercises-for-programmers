use std::io::{self, BufRead, BufReader, Write};

fn main() -> io::Result<()> {
    print!("What is the input string? ");
    io::stdout().flush().unwrap();

    let mut reader = BufReader::new(io::stdin());
    let mut buffer = String::new();
    reader.read_line(&mut buffer)?;

    buffer.pop();
    println!("{} has {} characters.", buffer, buffer.len());

    Ok(())
}
