# exercises-for-programmers

An implementation of the exercises described in the book "Exercises for Programmers" by Brian P. Hogan. The exercises' implementation is (or will be) given in various languages. This repository is mainly a playground for various languages. 